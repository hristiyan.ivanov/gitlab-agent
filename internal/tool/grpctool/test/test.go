package test

import (
	"context"
)

var (
	_ TestingServer = &GRPCTestingServer{}
)

type GRPCTestingServer struct {
	UnimplementedTestingServer
	UnaryFunc     func(context.Context, *Request) (*Response, error)
	StreamingFunc func(Testing_StreamingRequestResponseServer) error
}

func (s *GRPCTestingServer) RequestResponse(ctx context.Context, request *Request) (*Response, error) {
	return s.UnaryFunc(ctx, request)
}

func (s *GRPCTestingServer) StreamingRequestResponse(server Testing_StreamingRequestResponseServer) error {
	return s.StreamingFunc(server)
}
