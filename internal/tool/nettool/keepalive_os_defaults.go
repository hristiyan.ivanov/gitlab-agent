package nettool

import (
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"net"
	"syscall"
	"time"
)

// useOSTCPKeepAlive indicates that the OS keep alive configuration should be used.
// The negative value has the effect that the Go stdlib won't override the OS
// settings for the keepalive time and interval.
const useOSTCPKeepAlive time.Duration = -1

// controlTCPKeepAlive sets the SO_KEEPALIVE option on sockets
// This function is meant to be used by a Dialer or Listener
// when the client socket is created or when a connection is
// accepted.
// This option enables keepalive with whatever settings are configured
// for the dial or listen.
// In combination with useOSTCPKeepAlive (which sets a negative value)
// the Go stdlib will use the OS default keepalive settings.
func controlTCPKeepAlive(_, _ string, c syscall.RawConn) error {
	return c.Control(func(fd uintptr) {
		err := syscall.SetsockoptInt(int(fd), syscall.SOL_SOCKET, syscall.SO_KEEPALIVE, 1)
		if err != nil {
			panic(fmt.Errorf("failed to set SO_KEEPALIVE option on socket: %w", err))
		}
	})
}

func ListenConfigWithOSTCPKeepAlive() *net.ListenConfig {
	return &net.ListenConfig{
		KeepAlive: useOSTCPKeepAlive,
		Control:   controlTCPKeepAlive,
	}
}

func ListenWithOSTCPKeepAlive(network, address string) (net.Listener, error) {
	lc := ListenConfigWithOSTCPKeepAlive()
	return lc.Listen(context.Background(), network, address)
}

func TLSListenWithOSTCPKeepAlive(network, laddr string, config *tls.Config) (net.Listener, error) {
	if config == nil || len(config.Certificates) == 0 &&
		config.GetCertificate == nil && config.GetConfigForClient == nil {
		return nil, errors.New("tls: neither Certificates, GetCertificate, nor GetConfigForClient set in Config")
	}
	l, err := ListenWithOSTCPKeepAlive(network, laddr)
	if err != nil {
		return nil, err
	}
	return tls.NewListener(l, config), nil
}
