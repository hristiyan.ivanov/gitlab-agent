package redistool

import (
	"context"
	"fmt"
	"time"
	"unsafe"

	"github.com/redis/rueidis"
	"go.opentelemetry.io/otel/attribute"
	otelmetric "go.opentelemetry.io/otel/metric"
	"google.golang.org/protobuf/proto"
)

const (
	scanAndGCDeletedKeysMetricName               = "redis_expiring_hash_api_scan_and_gc_deleted_keys_count"
	expiringHashNameKey            attribute.Key = "expiring_hash_name"
)

// ExpiringHashAPI represents a low-level API to work with a two-level hash: key K1 -> hashKey K2 -> value []byte.
// key identifies the hash; hashKey identifies the key in the hash; value is the value for the hashKey.
type ExpiringHashAPI[K1 any, K2 any] interface {
	SetBuilder() SetBuilder[K1, K2]
	Unset(ctx context.Context, key K1, hashKey K2) error
	ScanAndGC(ctx context.Context, key K1, cb ScanAndGCCallback) (int /* keysDeleted */, error)
}

type ScanAndGCCallback func(rawHashKey string, value []byte, err error) (bool /* done */, error)

type RedisExpiringHashAPI[K1 any, K2 any] struct {
	Client                    rueidis.Client
	Key1ToRedisKey            KeyToRedisKey[K1]
	Key2ToRedisKey            KeyToRedisKey[K2]
	gcCounter                 otelmetric.Int64Counter
	gcCounterRecordAttributes attribute.Set
}

func NewRedisExpiringHashAPI[K1 any, K2 any](name string, client rueidis.Client, key1ToRedisKey KeyToRedisKey[K1], key2ToRedisKey KeyToRedisKey[K2], m otelmetric.Meter) (*RedisExpiringHashAPI[K1, K2], error) {
	gcCounter, err := m.Int64Counter(
		scanAndGCDeletedKeysMetricName,
		otelmetric.WithDescription("Amount of keys that have been garbage collected in a single 'ScanAndGC' pass"),
	)
	if err != nil {
		return nil, err
	}

	return &RedisExpiringHashAPI[K1, K2]{
		Client:                    client,
		Key1ToRedisKey:            key1ToRedisKey,
		Key2ToRedisKey:            key2ToRedisKey,
		gcCounter:                 gcCounter,
		gcCounterRecordAttributes: attribute.NewSet(expiringHashNameKey.String(name)),
	}, nil
}

func (h *RedisExpiringHashAPI[K1, K2]) SetBuilder() SetBuilder[K1, K2] {
	return &RedisSetBuilder[K1, K2]{
		client:         h.Client,
		key1ToRedisKey: h.Key1ToRedisKey,
		key2ToRedisKey: h.Key2ToRedisKey,
	}
}

func (h *RedisExpiringHashAPI[K1, K2]) Unset(ctx context.Context, key K1, hashKey K2) error {
	hdelCmd := h.Client.B().Hdel().Key(h.Key1ToRedisKey(key)).Field(h.Key2ToRedisKey(hashKey)).Build()
	return h.Client.Do(ctx, hdelCmd).Error()
}

func (h *RedisExpiringHashAPI[K1, K2]) ScanAndGC(ctx context.Context, key K1, cb ScanAndGCCallback) (int /* keysDeleted */, error) {
	now := time.Now().Unix()
	redisKey := h.Key1ToRedisKey(key)
	var keysToDelete []string
	scanErr := scan(ctx, redisKey, h.Client,
		func(k, v string) (bool /*done*/, error) {
			var msg ExpiringValue
			// Avoid creating a temporary copy
			vBytes := unsafe.Slice(unsafe.StringData(v), len(v)) //nolint: gosec
			err := proto.Unmarshal(vBytes, &msg)
			if err != nil {
				done, cbErr := cb(k, nil, fmt.Errorf("failed to unmarshal hash value from hashkey 0x%x: %w", k, err))
				return done, cbErr
			}
			if msg.ExpiresAt < now {
				keysToDelete = append(keysToDelete, k)
				return false, nil
			}
			done, cbErr := cb(k, msg.Value, nil)
			return done, cbErr
		})
	if len(keysToDelete) == 0 {
		return 0, scanErr
	}
	hdelCmd := h.Client.B().Hdel().Key(redisKey).Field(keysToDelete...).Build()
	err := h.Client.Do(ctx, hdelCmd).Error()
	if err != nil {
		if scanErr != nil {
			return 0, scanErr
		}
		return 0, err
	}
	// NOTE: only record keysToDelete if the deletion was actually successful.
	h.gcCounter.Add(ctx, int64(len(keysToDelete)), otelmetric.WithAttributeSet(h.gcCounterRecordAttributes))
	return len(keysToDelete), scanErr
}
