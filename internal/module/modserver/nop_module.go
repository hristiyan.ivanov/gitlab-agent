package modserver

import (
	"context"
)

type NopModule struct {
	ModuleName string
}

func (m *NopModule) Run(ctx context.Context) error {
	return nil
}

func (m *NopModule) Name() string {
	return m.ModuleName
}
