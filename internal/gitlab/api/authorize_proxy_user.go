package api

import (
	"context"
	"net/http"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v16/internal/gitlab"
)

const (
	AuthorizeProxyUserAPIPath = "/api/v4/internal/kubernetes/authorize_proxy_user"
)

func AuthorizeProxyUser(ctx context.Context, client gitlab.ClientInterface, agentID int64, accessType, accessKey, csrfToken string, opts ...gitlab.DoOption) (*AuthorizeProxyUserResponse, error) {
	auth := &AuthorizeProxyUserResponse{}
	err := client.Do(ctx,
		joinOpts(opts,
			gitlab.WithMethod(http.MethodPost),
			gitlab.WithPath(AuthorizeProxyUserAPIPath),
			gitlab.WithJWT(true),
			gitlab.WithProtoJSONRequestBody(&AuthorizeProxyUserRequest{
				AgentId:    agentID,
				AccessType: accessType,
				AccessKey:  accessKey,
				CsrfToken:  csrfToken,
			}),
			gitlab.WithResponseHandler(gitlab.ProtoJSONResponseHandlerWithStructuredErrReason(auth)),
		)...,
	)
	if err != nil {
		return nil, err
	}
	return auth, nil
}
